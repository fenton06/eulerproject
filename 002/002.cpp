/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on June 27, 2011, 1:16 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int fib1 = 1;
    int fib2 = 2;
    int sum = 2;
    
    for (int fib = 0; fib < 4000000; fib = fib1+fib2) {
        fib = fib1 + fib2;
        
        if (fib%2==0){
            sum = sum+fib;
            fib1 = fib2;
            fib2 = fib;
        }
        else{
            fib1 = fib2;
            fib2 = fib;
        }
    }
    
    cout << sum;
    
    return 0;
}

