/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on June 28, 2011, 7:48 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    long long int num = 600851475143LL;
    int i = 2;
    long long int fact = 0LL;
    
    do{
        if (num%i==0) {        
            fact = num/i;
            num=fact;
        }
        else {
            i++;
        }
    } while(num/i!=1);
    
    cout << num;

    return 0;
}

