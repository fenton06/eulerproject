/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on June 28, 2011, 10:28 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int i,j;
    int num1 = 1;
    int num1_orig = 1;
    int num1_rev = 1;
    int num2 = 1;
    int pal = 0;
    
    int reverse(int);
    
    for(i=1;i<1000;i++)
    {
        for(j=1;j<1000;j++)
        {
           num1 = i*j;
           
           num1_orig=num1;
           
           num1_rev = reverse(num1);
           
           if(num1_rev==num1_orig)
           {
               pal = num1_rev;
               if(pal>num2)
                   num2=pal;
           }
        }
    }
    
    cout << num2;

    return 0;
}

int reverse(int num1){
    
    int digit1, digit2, digit3, digit4, digit5, digit6, num1_rev;
    
    if (num1<10){
        num1_rev = num1;
        return num1_rev;
    } else if (num1<100){        
        digit1 = num1/10;
        digit2 = num1%10;
        num1_rev = digit2*10 + digit1;
        return num1_rev;
    } else if (num1<1000) {
        digit1 = num1/100;
        num1 = num1%100;
        digit2 = num1/10;
        digit3 = num1%10;
        num1_rev = digit3*100 + digit2*10 + digit1;
        return num1_rev;
    } else if (num1<10000) {
        digit1 = num1/1000;
        num1 = num1%1000;
        digit2 = num1/100;
        num1 = num1%100;
        digit3 = num1/10;
        digit4 = num1%10;
        num1_rev = digit4*1000 + digit3*100 + digit2*10 + digit1;
        return num1_rev;
    } else if (num1<100000) {
        digit1 = num1/10000;
        num1 = num1%10000;
        digit2 = num1/1000;
        num1 = num1%1000;
        digit3 = num1/100;
        num1 = num1%100;
        digit4 = num1/10;
        digit5 = num1%10;
        num1_rev = digit5*10000 + digit4*1000 + digit3*100 + digit2*10 + digit1;
        return num1_rev;
    } else if (num1<1000000) {
        digit1 = num1/100000;
        num1 = num1%100000;
        digit2 = num1/10000;
        num1 = num1%10000;
        digit3 = num1/1000;
        num1 = num1%1000;
        digit4 = num1/100;
        num1 = num1%100;
        digit5 = num1/10;
        digit6 = num1%10;
        num1_rev = digit6*100000 + digit5*10000 + digit4*1000 + digit3*100 + digit2*10 + digit1;
        return num1_rev;
    }
    
    return 0;
    }

