/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on June 28, 2011, 12:59 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int i, j, k;
    int num = 0;
    int erato [105000];
    
    // Initializes the array with the numbers
    
    for (i=0; i<=105000; i++){
        erato[i] = i+2;
    }   
    // Starts with the first nonzero number and makes multiples a zero
    
    i=1;
    j=0;
    
    do {                                        // counter for prime numbers, i is # of primes, j is position in array of prime
                                                // k is amount to increase j by
        if (erato[j]!=0) {                      // checks if number is non-zero    
            num = erato[j];                     // sets increment to non-zero number
            for (k=j+num; k<105000; k=k+num) {  // loop to replace non-primes with zero
            erato[k] = 0;                       
            }
            i++;
        }
        j++;
    } while (i<=10001);
    
    cout << num;
    
    return 0;
}

