/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on June 29, 2011, 11:06 AM
 */

#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int k, n, m;
    long long int sum;
    
    n = 2000000;                        // upper limit
    sum = 5LL;                          //start at 5 because 2 and 3 are prime
    
    for (int i=5;i<n; i=i+2) {          // loop that counts between 5 and n because we know 2 and 3 are prime is a prime number
        m = sqrt(i)+1;                  // m is the square root of the number we are testing for prime
        k=0;
        for (int j=2; j<=m; j++) {      // this loop *should* divide i from j to m            
            if (i%j==0) {               // checks if i is evenly divisible by numbers j, meaning not prime
                k++;                    // counter to make not prime
            }
        }        
        if(k==0) {                      // checks for prime since no numbers would have evenly divided
            sum = sum+i;                // adds prime number to previous prime number, starting at 5
        }
    }    
    cout << sum;    
 return 0;
}

