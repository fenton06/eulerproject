/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on July 20, 2011, 2:04 PM
 */

#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int factors=0;
    int triangle;
    int limit;
    
    for(int i=1; factors<500; i++){
        factors = 0;
        triangle = (i*(i+1))/2;
        limit = sqrt(triangle);
        for(int j=1; j<=limit; j++){
            if(triangle%j==0){
                factors+=2;
            }
        }
    }
    
    cout << triangle;

    return 0;
}

