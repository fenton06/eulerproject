/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on July 19, 2011, 11:36 AM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

int main(int argc, char** argv) {
    
    char nums_c [100][50];
    int i=0;
    int j=0;
    int k=0;
    string line;
    int nums [100] [50];
    vector<int> sum;
    int carry=0;
    int add_sum;

    ifstream myfile ("nums.txt");
      if (myfile.is_open())
      {
        while (myfile.good())
        {
          getline (myfile,line);
          strcpy(nums_c[i], line.c_str());
          i++;
        }
        myfile.close();
      }
      else cout << "Unable to open file";
//    for (j=0; j<100;j++) {
//        for (i=0; i<50; i++){
//            cout << nums_c[j][i];
//        }
//        cout << endl;
//    }
//
//    cout << "********************************" << endl;

    for (j=0; j<100;j++) {

        for (i=0, k=49; i<50; i++, k--){
            nums[j][k] = (int)(nums_c[j][i])-48;
        }
    }

//    for (j=0; j<100;j++) {
//        for (i=0; i<50; i++){
//            cout << nums[j][i];
//        }
//        cout << endl;
//    }
    
    for (i=0; i<50; i++) {
        add_sum = carry;        
        for (j=0; j<100; j++) {
            add_sum += nums[j][i];
        }
        carry = add_sum/10;
        sum.push_back(add_sum%10);
    }
    
    sum.push_back(carry);
    
    reverse(sum.begin(),sum.end());
    
    for (int i=0; i<9; i++) {
        cout << sum[i];
    }

    return 0;
}

