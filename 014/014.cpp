/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on July 20, 2011, 1:07 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    long long num;
    int terms_temp;
    int terms = 0;
    int longest;
    
    for (int i =500001; i<1000000; i+=2){
        num = i;
        terms_temp=1;
        while (num!=1){
            if  (num%2==0){
                num/=2;
                terms_temp++;
            }
            else {
                num = 3*num + 1;
                terms_temp++;
            }
        }
        if (terms_temp>terms){
            terms = terms_temp;
            longest = i;
        }
        
    }
    cout << longest << "**" << terms;
    return 0;
}

