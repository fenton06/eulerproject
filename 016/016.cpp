/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on June 29, 2011, 3:00 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <cmath>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   
    vector<int> digits;
    digits.push_back(1);

    for (int i=1;i<=15;i++) {

        int carry = 0;

        for(vector<int>::iterator iter = digits.begin(); iter !=digits.end(); iter++) {

            *iter = *iter*2 + carry;
            
            if(*iter>9 && (iter+1)!= digits.end()) {
                carry = *iter/10;
                *iter%=10;                
            } else if (*iter>9 && (iter+1) == digits.end()) {
                *iter%=10;
                digits.push_back(1);
                break;
            } else if(*iter<=9) {
                carry=0;
            }        
        }    
    }

    int sum = 0;

    for (vector<int>::iterator iter = digits.begin(); iter != digits.end(); iter++) {
        sum += *iter;
    }
            
    cout << sum;
    
    return 0;
}

