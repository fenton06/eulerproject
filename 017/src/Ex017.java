public class Ex017 {

    public static void main(String[] args) {

        int count = 0;

        for(int i = 1; i <=1000; i++) {

            int num = i;
//            int num = 1000;

            int remainder;

            // Numbers >= 1000
            if(num / 1000 > 0) {

                num = num % 1000;

                // one thousand = 11 letters
                count += 11;
            }

            // Numbers < 1000 and >= 100
            if(num / 100 > 0) {

                remainder = num % 100;
                num = num / 100;

                switch(num) {
                    case 1:
                    case 2:
                    case 6: count += 3;
                            break;

                    case 4:
                    case 5:
                    case 9: count += 4;
                            break;

                    case 3:
                    case 7:
                    case 8: count += 5;
                            break;
                }

                // Reassign num
                num = remainder;

                // Add the "hundred" count (7 letters)
                count += 7;

                if(num != 0) {
                    count += 3;
                }

            }

            // Numbers >= 20 and < 100
            if(num / 10 > 1) {

                remainder = num % 10;
                num = num / 10;

                switch(num) {
                    case 4:
                    case 5:
                    case 6: count += 5;
                            break;

                    case 2:
                    case 3:
                    case 8:
                    case 9: count += 6;
                            break;

                    case 7: count += 7;
                            break;
                }

                // Reassign num
                num = remainder;
            }

            // Numbers >= 10 and < 20
            if(num / 10 == 1) {

                remainder = num % 10;

                switch(remainder) {
                    case 0: count += 3;
                            break;

                    case 1:
                    case 2: count += 6;
                            break;

                    case 5:
                    case 6: count +=7;
                            break;

                    case 3:
                    case 4:
                    case 8:
                    case 9: count += 8;
                            break;

                    case 7: count += 9;
                            break;
                }

                // Reassign num
                num = 0;
            }

            if(num != 0) {

                remainder = num % 10;

                switch(remainder) {

                    case 1:
                    case 2:
                    case 6: count += 3;
                            break;

                    case 4:
                    case 5:
                    case 9: count += 4;
                            break;

                    case 3:
                    case 7:
                    case 8: count += 5;
                            break;
                }
            }
        }

        System.out.println(count);
    }
}
