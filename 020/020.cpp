/* 
 * File:   main.cpp
 * Author: BLF06
 *
 * Created on June 30, 2011, 9:58 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    vector<int> digits;
    digits.push_back(1);

    for (int i=1;i<=100;i++) {       
        
        for (int j=0; j < digits.size(); j++){
                cout << digits[j];
            }
            cout << endl;        
        
        int carry = 0;
        
        cout << i <<". ";
        
        for(vector<int>::iterator iter = digits.begin(); iter !=digits.end(); iter++) {
            *iter = *iter*i + carry;
            if(*iter>9 && (iter+1)!= digits.end()) {
                carry=*iter/10;
                *iter%=10;
            } else if (*iter>9 && (iter+1) == digits.end()) {
                carry=*iter/10;
                *iter%=10;
                if(carry>=10) {
                    int rem = carry%10;
                    digits.push_back(rem);
                    carry/=10;
                }
                digits.push_back(carry);
                break;
            } else if(*iter<=9) {
                carry=0;
            }        
        }    
    }
    
    for (int j=0; j < digits.size(); j++){
                cout << digits[j];
            }
            
            cout << endl;

    int sum = 0;

    for (int i=0; i<digits.size(); i++) {
        sum = sum+digits[i];
    }
            
    cout << sum;
    
    return 0;
}

