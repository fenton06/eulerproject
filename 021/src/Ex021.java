public class Ex021 {

    public static void main(String[] args) {

        int sum = 0;
        int high = 10000;

        // Check if i is amiable and add to sum if so
        for(int i = 1; i <= high; i++) {
            if(checkAmicable(i)) {
                sum += i;
            }
        }

        System.out.println(sum);

    }

    private static boolean checkAmicable(int num) {

        return (num == divisorSum(divisorSum(num))) && (num != divisorSum(num));

    }

    private static int divisorSum(int num) {

        int sum = 0;

        for(int i = 1; i < num; i++) {
            // Check evenly divisible
            if(num % i == 0) {
                sum += i;
            }
        }

        return sum;
    }
}

