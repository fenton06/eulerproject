
import java.util.ArrayList;

public class Ex025 {

    public static void main(String[] args) {

        ArrayList<Integer> topArr = new ArrayList<>();
        ArrayList<Integer> bottomArr = new ArrayList<>();
        ArrayList<Integer> sumArr = new ArrayList<>();

        topArr.add(1);
        bottomArr.add(1);

        int count = 2;

        while(bottomArr.size() < 1000) {

            for(int i = 0; i < bottomArr.size(); i++) {

                int sum;
                boolean carry = false;

                // Check for size
                if(i < topArr.size()) {
                     sum = topArr.get(i) + bottomArr.get(i);
                } else {
                    sum = bottomArr.get(i);
                }

                if(sum / 10 == 1) {
                    sum = sum % 10;
                    carry = true;
                }

                if(i == sumArr.size() - 1) {  //  Previous carry present

                    if(carry) {
                        sumArr.set(i, sumArr.get(i) + sum);
                        sumArr.add(1);
                    } else {
                        sumArr.set(i, sumArr.get(i) + sum);
                    }

                } else {  //  No previous carry

                    if(carry) {
                        sumArr.add(sum);
                        sumArr.add(1);
                    } else {
                        sumArr.add(sum);
                    }

                }

                if(sumArr.get(i) / 10 == 1) {
                    sumArr.set(i, sumArr.get(i) % 10);
                    sumArr.add(1);
                }
            }

            topArr = bottomArr;
            bottomArr = sumArr;
            sumArr = new ArrayList<>();

            count++;

        }

        System.out.println(count);
    }
}
