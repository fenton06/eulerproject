﻿using System;

namespace P019
{
    class Program
    {
        static void Main(string[] args)
        {

            var date = new DateTime(1900, 12,31);

            var numberOfMondays = 0;
            
            while (date.Year < 2001)
            {
                date = date.AddDays(1);

                if (date.Day == 1 && date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    numberOfMondays++;
                }
            }

            Console.WriteLine(numberOfMondays);
        }
    }
}
