﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P022
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var text = System.IO.File.ReadAllText(@"/Users/bfenton/Projects/EulerProject/P022/names.txt");
            
            var nameListDirty = text.Split(',').ToList();

            var nameList = sanitize(nameListDirty);
            
            nameList.Sort();

            var nameListSum = 0;
            
            for (var i = 1; i <= nameList.Count; i++)
            {
                var name = nameList[i-1];
                var nameSum = 0;
                
                foreach (var c in name)
                {
                    var idx = char.ToUpper(c) - 64;
                    nameSum += idx;
                }

                nameListSum += i * nameSum;
            }

            Console.WriteLine(nameListSum);
        }

        private static List<string> sanitize(IEnumerable<string> dirtyList)
        {
            var cleanList = new List<string>();
            
            foreach(var nameStr in dirtyList)
            {
                cleanList.Add(nameStr.Substring(1, nameStr.Length - 2));
            }

            return cleanList;
        }
    }
}