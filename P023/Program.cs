﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P023
{
    class Program
    {
        static void Main(string[] args)
        {

            var abundantNumbers = findAbundantNumbers();

            var sumOfTwoAbundantDivisorsBools = determineIfSumOfAbundantNumbers(abundantNumbers);

            var notASumOfTwoAbundants = getNumbersNotASum(sumOfTwoAbundantDivisorsBools);

            var sumOfNonSums = getNonSumSum(notASumOfTwoAbundants);

            Console.WriteLine(sumOfNonSums);
        }

        private static List<int> findAbundantNumbers()
        {
            var abundantNumbers = new List<int>();

            for (var i = 1; i <= 28123; i++)
            {

                var properDivisors = findProperDivisors(i);

                var sumOfDivisors = sumDivisors(properDivisors);

                if (sumOfDivisors > i)
                {
                    abundantNumbers.Add(i);
                }
            }

            return abundantNumbers;
        }

        private static IEnumerable<int> findProperDivisors(int i)
        {
            var properDivisors = new List<int>();

            // find proper divisors
            for (var j = 1; j < i; j++)
            {
                if (i % j == 0)
                {
                    properDivisors.Add(j);
                }
            }

            return properDivisors;
        }

        private static int sumDivisors(IEnumerable<int> properDivisors)
        {
            var sumOfDivisors = 0;
            
            // Sum proper divisors
            foreach (var divisor in properDivisors)
            {
                sumOfDivisors += divisor;
            }

            return sumOfDivisors;
        }

        private static bool[] determineIfSumOfAbundantNumbers(List<int> abundantNumbers)
        {
            var sumOfTwoAbundantDivisors = new bool[28124];
            
            for (var i = 0; i < abundantNumbers.Count; i++)
            {
                for (var j = i; j < abundantNumbers.Count; j++)
                {
                    var idx = abundantNumbers[i] + abundantNumbers[j];

                    if (idx < sumOfTwoAbundantDivisors.Length)
                    {
                        sumOfTwoAbundantDivisors[idx] = true;
                    }
                }
            }

            return sumOfTwoAbundantDivisors;
        }

        private static List<int> getNumbersNotASum(IReadOnlyList<bool> sumOfTwoAbundantDivisorsBools)
        {
            var sumOfTwoAbundants = new List<int>();

            for (var i = 0; i < sumOfTwoAbundantDivisorsBools.Count; i++)
            {
                if (sumOfTwoAbundantDivisorsBools[i] != true)
                {
                    sumOfTwoAbundants.Add(i);
                }
            }

            return sumOfTwoAbundants;
        }

        private static int getNonSumSum(List<int> notASumOfTwoAbundants)
        {
            var sumOfNonSums = 0;

            foreach (var num in notASumOfTwoAbundants)
            {
                sumOfNonSums += num;
            }

            return sumOfNonSums;
        }
    }
}
