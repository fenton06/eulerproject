﻿using System;
using System.Collections.Generic;

namespace P024
{
    class Program
    {
        private static List<string> permutations = new List<string>();
        
        static void Main(string[] args)
        {
            
            var nums = new []{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

            generate(nums.Length, nums);
            
            permutations.Sort();

            Console.WriteLine(permutations[1000000 - 1]);
        }

        private static void generate(int size, IList<string> arr)
        {
            if (size == 1)
            {
                var permutation = "";
                
                foreach(var item in arr)
                {
                    permutation += item;
                }
                
                permutations.Add(permutation);
            }
            else
            {
                generate(size - 1, arr);

                for (var i = 0; i < size - 1; i++)
                {
                    if (size % 2 == 0)
                    {
                        var temp = arr[i];
                        arr[i] = arr[size - 1];
                        arr[size - 1] = temp;
                    }
                    else
                    {
                        var temp = arr[0];
                        arr[0] = arr[size - 1];
                        arr[size - 1] = temp;
                    }

                    generate(size - 1, arr);
                }
            }
            
        }
    }
}
