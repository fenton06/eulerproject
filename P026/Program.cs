﻿using System;

namespace P026
{
    class Program
    {
        static void Main(string[] args)
        {
            var longestCycleNumber = 0;
            var longestCycle = 0;

            for (var i = 2; i < 1000; i++)
            {
                var cycleLength = getCycleLength(i);

                if (cycleLength > longestCycle)
                {
                    longestCycle = cycleLength;
                    longestCycleNumber = i;
                }
            }

            Console.WriteLine(longestCycleNumber);
        }
        
        private static int getCycleLength(int denominator)
        {
            var maxIters = 10000;
            var iters = 0;
            var a = 1;
            var length = 0;
            
            do
            {
                if (iters > maxIters)
                {
                    return 0;
                }
                
                a = a * 10 % denominator;
                length++;
                iters++;

            } while (a != 1);
            
            return length;
        }
    }
}