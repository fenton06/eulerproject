﻿using System;

namespace P027
{
    class Program
    {
        static void Main(string[] args)
        {

            var aMax = 0;
            var bMax = 0;
            var nMax = 0;

            for (var a = -999; a < 1000; a++)
            {
                for (var b = -1000; b <= 1000; b++)
                {
                    var n = 0;

                    while (isPrime(Math.Abs(n * n + n * a + b)))
                    {
                        n++;
                    }

                    if (n > nMax)
                    {
                        aMax = a;
                        bMax = b;
                        nMax = n;
                    }
                }
            }
            
            Console.WriteLine(aMax * bMax);
        }

        private static bool isPrime(int n)
        {
            if (n <= 3)
            {
                return n > 1;
            }
            
            if (n % 2 == 0 || n % 3 == 0)
            {
                return false;
            }

            var i = 5;

            while (i * i <= n)
            {
                if (n % i == 0 || n % (i + 2) == 0)
                {
                    return false;
                }

                i += 6;
            }

            return true;
        }

        private static int evaluateQuadratic(int a, int b)
        {


            return 0;
        }
    }
}
