﻿using System;

namespace P0228
{
    class Program
    {
        static void Main(string[] args)
        {
            var length = 1001;
            var numSquares = length / 2 + 1;

            var sumOfDiags = 0;

            for (var i = 0; i < numSquares; i++)
            {
                var lastNum = 4 * i * i + 4 * i + 1; // 4i^2 + 4i + 1

                sumOfDiags += lastNum;
                
                if (i != 0)
                {
                    for (var j = 1; j < 4; j++)
                    {
                        lastNum -= 2 * i;
                        
                        sumOfDiags += lastNum;
                    }
                }
                
            }
            
            Console.WriteLine(sumOfDiags);
        }
    }
}
