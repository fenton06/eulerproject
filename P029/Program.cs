﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace P029
{
    class Program
    {
        static void Main(string[] args)
        {

            var powerSet = new HashSet<BigInteger>();
            
            for (var i = 2; i <= 100; i++)
            {
                for (var j = 2; j <= 100; j++)
                {
                    powerSet.Add(BigInteger.Pow(i, j));
                }
            }
            
            Console.WriteLine(powerSet.Count);
        }
    }
}