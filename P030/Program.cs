﻿using System;

namespace P030
{
    class Program
    {
        static void Main(string[] args)
        {

            var result = 0;
            
            for (var i = 2; i < 355000; i++)
            {

                var sumOfPowers = 0;
                var number = i;

                while (number > 0)
                {
                    var d = number % 10;
                    number /= 10;

                    var temp = d;

                    for (var j = 1; j < 5; j++)
                    {
                        temp *= d;
                    }

                    sumOfPowers += temp;
                }

                if (sumOfPowers == i)
                {
                    result += i;
                }
            }
            
            Console.WriteLine(result);
        }
    }
}
