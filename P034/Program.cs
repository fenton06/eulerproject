﻿using System;

namespace P034
{
    class Program
    {
        static void Main(string[] args)
        {
            var sum = 0;
            var facts = new int[10];
            
            // Precalc factorials
            for (var i = 0; i < 10; i++) {
                facts[i] = factorial(i);
            }

            for (var i = 3; i < 2540160; i++)
            {
                var sumOfFactorials = 0;
                var  number = i;
                
                while (number > 0) {
                    var d = number % 10;
                    number /= 10;
                    sumOfFactorials += facts[d];
                }

                if (sumOfFactorials == i)
                {
                    sum += i;
                }
            }

            Console.WriteLine(sum);
        }

        private static int factorial(int number)
        {
            var sum = 1;

            for (var i = 2; i <= number; i++)
            {
                sum *= i;
            }

            return sum;
        }
    }
}
