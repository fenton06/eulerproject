﻿using System;
using System.Collections.Generic;

namespace P035
{
    class Program
    {
        static void Main(string[] args)
        {
            var sieve = generateSieve(1000000);

            var circularPrimes = new List<int>();

            for (var i = 2; i < 1000000; i++)
            {
                if (sieve[i] && isCircularPrime(sieve, i))
                {
                    circularPrimes.Add(i);
                }
            }
            
            Console.WriteLine(circularPrimes.Count);
        }

        private static bool[] generateSieve(int n)
        {
            var sieve = new bool[n+1];

            for (var i = 0; i < n; i++)
            {
                sieve[i] = true; 
            }

            for(var p = 2; p * p <= n; p++) 
            {
                if(sieve[p]) 
                {
                    for (var i = p * p; i <= n; i += p)
                    {
                        sieve[i] = false;
                    }
                } 
            }

            return sieve;
        }

        private static bool isCircularPrime(bool[] sieve, int num)
        {
            var numStr = num.ToString();
            var circularArr = new string[numStr.Length];

            for (var i = 0; i < numStr.Length; i++)
            {
                numStr = numStr.Substring(1) + numStr.Substring(0, 1);
                circularArr[i] = numStr;
            }

            var allPrime = true;
            
            foreach (var number in circularArr)
            {
                if (!sieve[int.Parse(number)])
                {
                    allPrime = false;
                    break;
                }
            }

            return allPrime;
        }
    }
}
