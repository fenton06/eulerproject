﻿using System;

namespace P036
{
    class Program
    {
        static void Main(string[] args)
        {

            var sum = 0;

            for (var i = 0; i < 1000000; i++)
            {
                if (isPalindromeString(i, 10) && isPalindromeString(i, 2))
                {
                    sum += i;
                }
            }

            Console.WriteLine(sum);
        }
        
        private static bool isPalindromeString(int number, int b) {
            var numStr = Convert.ToString(number,b);
            var arr = numStr.ToCharArray();
            Array.Reverse(arr);
            return numStr.Equals(new string(arr));
        }
    }
}
