﻿using System;
using System.Collections.Generic;

namespace P037
{
    class Program
    {
        static void Main(string[] args)
        {
            var primes = new HashSet<int>();
            var desiredPrimes = new List<int>();

            var i = 2;
            
            while (desiredPrimes.Count < 11)
            {
                if (IsPrime(i))
                {
                    primes.Add(i);
                    
                    if (i > 10 && IsRightTruncatable(primes, i) && IsLeftTruncatable(primes, i))
                    {
                        desiredPrimes.Add(i);
                    }
                }

                i++;
            }

            var sum = 0;

            foreach (var num in desiredPrimes)
            {
                sum += num;
            }

            Console.WriteLine(sum);
        }

        private static bool IsPrime(int n)
        {
            if (n <= 3)
            {
                return n > 1;
            }
            
            if (n % 2 == 0 || n % 3 == 0)
            {
                return false;
            }

            var i = 5;
                
            while (i * i <= n)
            {
                if (n % i == 0 || n % (i + 2) == 0)
                {
                    return false;
                }

                i += 6;
            }

            return true;
        }

        private static bool IsRightTruncatable(HashSet<int> primes, int num)
        {
            var numStr = num.ToString();

            while (numStr.Length > 0)
            {
                if (!primes.Contains(int.Parse(numStr)))
                {
                    return false;
                }

                numStr = numStr.Substring(0, numStr.Length - 1);
            }

            return true;
        }

        private static bool IsLeftTruncatable(HashSet<int> primes, int num)
        {
            var numStr = num.ToString();
            
            while (numStr.Length > 0)
            {
                if (!primes.Contains(int.Parse(numStr)))
                {
                    return false;
                }

                numStr = numStr.Substring(1, numStr.Length - 1);
            }

            return true;
        }
    }
}
