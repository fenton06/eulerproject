﻿using System;
using System.Collections.Generic;

namespace P039
{
    class Program
    {
        static void Main(string[] args)
        {
            var maxP = 0;
            var numSolutions = 0;
 
            for (var p = 2; p <= 1000; p += 2) {
                
                var tempNumSolutions = 0;
                
                for (var a = 2; a < p / 3; a++)
                {
                    if (p * (p - 2 * a) % (2 * (p - a)) == 0)
                    {
                        tempNumSolutions++;
                        
                    }
                }

                if(tempNumSolutions > numSolutions){
                    numSolutions = tempNumSolutions;
                    maxP = p;
                }
            }

            Console.WriteLine(maxP);
        }
    }
}
