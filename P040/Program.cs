﻿using System;
using System.Numerics;

namespace P040
{
    class Program
    {
        static void Main(string[] args)
        {

            var number = "";

            BigInteger i = 1;
            
            while (number.Length < 1000000)
            {
                number += i;

                i++;
            }

            var numberArr = number.ToCharArray();
            var product = 1;
            var j = 1;

            while (j <= 1000000)
            {
                var digit = int.Parse(numberArr[j-1].ToString());
                
                product *= digit;

                j *= 10;
            }

            Console.WriteLine(product);
        }
    }
}