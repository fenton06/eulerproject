﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P042
{
    class Program
    {
        static void Main(string[] args)
        {
            var text = System.IO.File.ReadAllText(@"/Users/bfenton/Projects/EulerProject/P042/p042_words.txt");
            var wordListDirty = text.Split(',').ToList();
            var wordList = Sanitize(wordListDirty);
            var wordListVals = ConvertWordsToNums(wordList);
            var largestNumber = FindLargestNumber(wordListVals);
            var triangleNumbers = CalculateTriangleNumbers(largestNumber);


            var numTriangleNumbers = 0;
            foreach (var wordNum in wordListVals)
            {
                if (triangleNumbers.Contains(wordNum))
                {
                    numTriangleNumbers++;
                }
            }

            Console.WriteLine(numTriangleNumbers);
        }
        
        private static IEnumerable<string> Sanitize(IEnumerable<string> dirtyList)
        {
            var cleanList = new List<string>();
            
            foreach(var nameStr in dirtyList)
            {
                cleanList.Add(nameStr.Substring(1, nameStr.Length - 2));
            }

            return cleanList;
        }
        
        private static List<int> ConvertWordsToNums(IEnumerable<string> wordList)
        {
            var wordListVals = new List<int>();

            foreach(var nameStr in wordList)
            {
                var sum = 0;
            
                foreach (var c in nameStr) {
                    
                    sum = sum + c - 'A' + 1;
                }
                
                wordListVals.Add(sum);
            }

            return wordListVals;
        }

        private static int FindLargestNumber(IEnumerable<int> wordListVals)
        {
            var largestNumber = 0;

            // Find largest number
            foreach (var number in wordListVals)
            {
                if (number > largestNumber)
                {
                    largestNumber = number;
                }
            }

            return largestNumber;
        }

        private static List<double> CalculateTriangleNumbers(int largestNumber)
        {
            var triangleNumbers = new List<double>();
            var i = 1;
            do
            {
                triangleNumbers.Add(.5 * i * (i + 1));

                i++;
            } while (triangleNumbers[triangleNumbers.Count - 1] <= largestNumber);

            return triangleNumbers;
        }
    }
}
