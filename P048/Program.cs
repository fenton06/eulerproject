﻿using System;
using System.Numerics;

namespace P048
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger sum = 0;

            for (var i = 1; i <= 1000; i++)
            {

                sum += BigInteger.Pow(i, i);
            }

            var sumStr = sum.ToString();

            Console.WriteLine(sumStr.Substring(sumStr.Length - 10));
        }
    }
}